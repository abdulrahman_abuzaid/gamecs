﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleCards;

namespace Assignment3
{
    /// <summary>
    /// blackjack game
    /// </summary>
    class Program
    {
        /// <summary>
        /// A simplified blackjack game
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // declares a deck and two hands for the player and the dealer
            BlackjackHand dealerHand = new BlackjackHand("dealer");
            BlackjackHand playerHand = new BlackjackHand("player");
            Deck deck = new Deck();

            // prints welcoming message
            Console.WriteLine("Welcome to this game of blackjack :)\n"+
                "We will now play a single hand of blackjack\n");

            // shuffles the deck
            deck.Shuffle();

            // deals two cards to each the player and the dealer
            dealerHand.AddCard(deck.TakeTopCard());
            dealerHand.AddCard(deck.TakeTopCard());
            playerHand.AddCard(deck.TakeTopCard());
            playerHand.AddCard(deck.TakeTopCard());

            // shows and prints all cards of the player
            // and the first card of the dealer
            playerHand.ShowAllCards();
            dealerHand.ShowFirstCard();
            dealerHand.Print();
            playerHand.Print();

            // to hit or not to hit!
            playerHand.HitOrNot(deck);

            // shows all cards of the dealer and reprints all cards
            dealerHand.ShowAllCards();
            dealerHand.Print();
            playerHand.Print();


            Console.WriteLine("Score of the dealer: " + dealerHand.Score);
            Console.WriteLine("Score of the player: " + playerHand.Score +"\n");
        }
    }
}
