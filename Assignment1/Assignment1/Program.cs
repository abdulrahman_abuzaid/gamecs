﻿using System;

namespace Assignment1
{
    /// <summary>
    /// Calculate Gold Rate
    /// </summary>
    class Program
    {
        /// <summary>
        /// Calculate Gold Rate
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Initializing parameters
            int gold;
            const int MINUTES_PER_HOUR = 60;
            float minutes;
            float hours;
            float goldPerMinute;

            // Welcoming comments
            Console.WriteLine("Hello adventurer!");
            Console.Write("This program will calculate how much ");
            Console.WriteLine("gold you collect per minute.");
            Console.WriteLine();

            // Reading input from the user
            Console.Write("Please enter the total amout of gold collected: ");
            gold = int.Parse(Console.ReadLine());
            Console.Write("Please enter the total hours played: ");
            hours = float.Parse(Console.ReadLine());

            // Calculating the gold per minute statistic
            minutes = hours * MINUTES_PER_HOUR;
            goldPerMinute = gold / minutes;

            // Printing out results
            Console.WriteLine();
            Console.WriteLine("Total gold collected: " + gold +
                "\nTotal hours played: " + hours +
                "\nGold per minute collection rate: "+
                goldPerMinute);
            Console.WriteLine();
        }
    }
}
