﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgrammingAssignment5
{
    class Program
    {
        static void Main(string[] args)
        {
            // variables declarations
            Random random = new Random();
            int player1Roll, player2Roll;
            int winCount1, winCount2;
            char newGame = 'Y';

            // prints welcome message
            Console.Out.WriteLine("Welcome to our game of WAR!\n");

            // main game loop
            while (newGame == 'Y')
            {
                // resets the win counters
                winCount1 = 0;
                winCount2 = 0;

                // rolls the dies 21 times
                for (int i = 1; i <= 21; i++)
                {
                    // rolls the 13-sided dies
                    player1Roll = random.Next(13) + 1;
                    player2Roll = random.Next(13) + 1;

                    // checks if it is WAR
                    while (player1Roll == player2Roll)
                    {
                        Console.WriteLine("  WAR!   P1:" + player1Roll +
                            "  P2:" + player2Roll);
                        player1Roll = random.Next(13) + 1;
                        player2Roll = random.Next(13) + 1;
                    }

                    // prints out the winner of this round
                    Console.Write("BATTLE:  P1:" + player1Roll +
                            "  P2:" + player2Roll + "    ");
                    if (player1Roll > player2Roll)
                    {
                        Console.WriteLine("P1 Wins!");
                        winCount1 += 1;
                    }
                    else
                    {
                        Console.WriteLine("P2 Wins!");
                        winCount2 += 1;
                    }
                }

                // prints out the winner of the current game
                Console.WriteLine();
                if (winCount1 > winCount2)
                {
                    Console.WriteLine("P1 is the overall Winner with " +
                        winCount1 + " battles");
                }
                else
                {
                    Console.WriteLine("P2 is the overall Winner with " +
                        winCount2 + " battles");
                }

                // checks if the player wants to play again
                Console.WriteLine();
                Console.Write("Do you want to play another game (y/n)? ");
                newGame = char.Parse(Console.ReadLine().ToUpper());

                // checks for incorrect input
                while (newGame != 'Y' &&
                    newGame != 'N')
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid input, must be (N/n) or (Y/y)");
                    Console.WriteLine();
                    Console.Write("Do you want to play another game (y/n)? ");
                    newGame = char.Parse(Console.ReadLine().ToUpper());
                }
                Console.WriteLine();
            }
        }
    }
}


