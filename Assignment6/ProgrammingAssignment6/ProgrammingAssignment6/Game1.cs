using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XnaCards;

namespace ProgrammingAssignment6
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        // keep track of game state and current winner
        static GameState gameState = GameState.Play;
        

        // hands and battle piles for the players
        WarBattlePile warBattlepile1;
        WarBattlePile warBattlepile2;
        WarHand warHand1;
        WarHand warHand2;
        Deck player1Deck;
        Deck player2Deck;

        // winner messages for players
        WinnerMessage winnerMessage1;
        WinnerMessage winnerMessage2;

        // menu buttons
        MenuButton flipButton;
        MenuButton quitButton;
        MenuButton collectWinningsButton;
 
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // make mouse visible and set resolution
            IsMouseVisible = true;
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // create the deck object and shuffle
            player1Deck = new Deck(Content, 450, 120);
            player1Deck.Shuffle();

            player2Deck = new Deck(Content, 450, 480);
            player2Deck.Shuffle();
            player2Deck.Shuffle();
            
           
            // create the player hands and fully deal the deck into the hands
            warHand1 = new WarHand(450, 120);
            warHand2 = new WarHand(450, 480);

            while (!player1Deck.Empty)
                warHand1.AddCard(player1Deck.TakeTopCard());

            while (!player2Deck.Empty)
                warHand2.AddCard(player2Deck.TakeTopCard());

            // create the player battle piles
            warBattlepile1 = new WarBattlePile(450, 250);
            warBattlepile2 = new WarBattlePile(450, 350);

            // create the player winner messages
            winnerMessage1 = new WinnerMessage(Content, 650, 120);
            winnerMessage2 = new WinnerMessage(Content, 650, 480);
            

            // create the menu button
            flipButton = new MenuButton(Content, "flipbutton", 200, 150,
                GameState.Flip);
            quitButton = new MenuButton(Content, "quitbutton", 200, 450,
                GameState.Quit);
            collectWinningsButton = new MenuButton(Content, "collectwinningsbutton",
                200, 150, GameState.CollectWinnings);
            collectWinningsButton.Visible = false;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            //
            MouseState mouse = Mouse.GetState();

            // update the menu buttons
            flipButton.Update(mouse);
            quitButton.Update(mouse);
            collectWinningsButton.Update(mouse);
 
            // update based on game state
            if (gameState == GameState.Flip)
            {
                winnerMessage1.Visible = false;
                winnerMessage2.Visible = false;
                warBattlepile1.AddCard(warHand1.TakeTopCard());
                warBattlepile1.GetTopCard().FlipOver();
                warBattlepile2.AddCard(warHand2.TakeTopCard());
                warBattlepile2.GetTopCard().FlipOver();
                gameState = GameState.Play;
                collectWinningsButton.Visible = true;
                flipButton.Visible = false;
            }
            else if (gameState == GameState.CollectWinnings)
            {
                if (warBattlepile1.GetTopCard().WarValue >
                    warBattlepile2.GetTopCard().WarValue)
                {
                    winnerMessage1.Visible = true;
                    winnerMessage2.Visible = false;
                    warHand1.AddCards(warBattlepile1);
                    warHand1.AddCards(warBattlepile2);
                }
                else if (warBattlepile1.GetTopCard().WarValue <
                    warBattlepile2.GetTopCard().WarValue)
                {
                    winnerMessage1.Visible = false;
                    winnerMessage2.Visible = true;
                    warHand2.AddCards(warBattlepile1);
                    warHand2.AddCards(warBattlepile2);
                }
                else
                {
                    warHand1.AddCards(warBattlepile1);
                    warHand2.AddCards(warBattlepile2);
                }
                collectWinningsButton.Visible = false;
                flipButton.Visible = true;
                if (warHand1.Empty || warHand2.Empty)
                {
                    gameState = GameState.GameOver;
                    flipButton.Visible = false;
                }
                else
                    gameState = GameState.Play;
            }
            if (gameState == GameState.Quit)
                this.Exit();
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Goldenrod);

            spriteBatch.Begin();
            
            // draw the game objects
            warHand1.Draw(spriteBatch);
            warBattlepile1.Draw(spriteBatch);
            warHand2.Draw(spriteBatch);
            warBattlepile2.Draw(spriteBatch);

            // draw the winner messages
            winnerMessage1.Draw(spriteBatch);
            winnerMessage2.Draw(spriteBatch);
 
            // draw the menu buttons
            flipButton.Draw(spriteBatch);
            quitButton.Draw(spriteBatch);
            collectWinningsButton.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Changes the state of the game
        /// </summary>
        /// <param name="newState">the new game state</param>
        public static void ChangeState(GameState newState)
        {
            gameState = newState;
        }
    }
}
